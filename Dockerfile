# Use the official PHP image with Apache as the web server
FROM php:8.1-apache

# Copy custom MySQL configuration
COPY my.cnf /etc/mysql/conf.d/my.cnf

# Set the working directory
WORKDIR /var/www/html

# Copy the application code into the container
COPY ./public /var/www/html/public
COPY ./templates /var/www/html/templates
COPY . .

# Install system dependencies
RUN apt-get update && apt-get install -y \
    libicu-dev \
    git \
    unzip

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql intl

# Enable Apache modules
RUN a2enmod rewrite
RUN a2enmod autoindex  # Enable autoindex module

# AllowOverride All for .htaccess to work
RUN sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Composer dependencies
RUN composer install --no-scripts --no-autoloader

# Install Composer dependencies with autoloader
RUN composer dump-autoload --optimize

# Create var/ directory
RUN mkdir -p var/

# Set permissions
RUN chown -R www-data:www-data var/

# Set ServerName directive to suppress the warning
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Expose the port
EXPOSE 80

# Start Apache server
CMD ["apache2-foreground"]
