<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Psr\Log\LoggerInterface;

/**
 * @group legacy
 */
class FunctionnalTest extends WebTestCase
{
    public function testClientIndex()
    {
        $client = static::createClient();

        // Create a mock LoggerInterface
        $logger = $this->createMock(LoggerInterface::class);

        // Send a request to the client
        $crawler = $client->request('GET', '/client/');

        // Assert that the response is successful
        $this->assertResponseIsSuccessful();

        // Assert the presence of the table
        $this->assertSelectorExists('table.table');

        // Assert that the table contains rows for clients
        $this->assertGreaterThan(0, $crawler->filter('table.table tbody tr')->count(), 'Le tableau doit contenir au moins une ligne pour un client.');

        // Assert the presence and content of column headers
        $this->assertSelectorTextContains('table.table thead tr th:first-child', 'Id');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(2)', 'Nom');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(3)', 'Prenom');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(4)', 'Adresse');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(5)', 'Cin');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(6)', 'actions');
    }

    /**
     * @group legacy
     */
    public function testLocationIndex()
    {
        $client = static::createClient();

        // Create a mock LoggerInterface
        $logger = $this->createMock(LoggerInterface::class);

        // Simulate a GET request to the location index
        $crawler = $client->request('GET', '/location/');

        // Assert that the response is successful
        $this->assertResponseIsSuccessful();

        // Assert the presence of the table
        $this->assertSelectorExists('table.table');

        // Assert that the table contains rows for locations
        $this->assertGreaterThan(0, $crawler->filter('table.table tbody tr')->count(), 'Le tableau doit contenir au moins une ligne pour une location.');

        // Assert the presence and content of column headers
        $this->assertSelectorTextContains('table.table thead tr th:first-child', 'Id');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(2)', 'DateD');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(3)', 'DateA');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(4)', 'Prix');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(5)', 'Client');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(6)', 'Voiture');
        $this->assertSelectorTextContains('table.table thead tr th:nth-child(7)', 'actions');

        // If no records found, assert a specific message
        if ($crawler->filter('table.table tbody tr')->count() == 0) {
            $this->assertSelectorTextContains('table.table tbody tr td', 'no records found');
        }
    }
}
