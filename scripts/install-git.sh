#!/bin/bash

# Set Git global configuration
if [ -z "$GITLAB_USER_EMAIL" ]; then
    echo "Error: GITLAB_USER_EMAIL is not set. Exiting..."
    exit 1
fi

git config --global user.email "$GITLAB_USER_EMAIL"

if [ -z "$GITLAB_USER_NAME" ]; then
    echo "Error: GITLAB_USER_NAME is not set. Exiting..."
    exit 1
fi

git config --global user.name "$GITLAB_USER_NAME"
